﻿using System;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Chrome;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace registration
{
    [TestFixture]
    public class TestCases
    {
        private static IWebDriver driver;
        private  static WebDriverWait wait;
        private static IList<IWebElement> errors;
        
        private static By
            errorLocator = By.CssSelector(".dEOOab.RxsGPe"),
            header = By.Id("headingText"),
            firstNameLocator = By.Id("firstName");

        private static IWebElement
            firstName,
            lastName,
            username,
            password,
            confirmPassword,
            nextBtn,
            confirmHeader,
            error;
        
        private static string
                    _firstName = "FirstName",
                    _lastName = "LastName",
                    _username = "jhv98HbjhH1",
                    _password = "jhv98HbjhH1*hj875t",
                    _passwordShort = "passw",
                    _passwordWeak = "11111111111",
                    _welcomeHeader = "Вітаємо в Google",
                    _confirmPhoneHeader = "Підтвердьте номер телефону",
                    _firstNameError = "Введіть ім’я",
                    _lastNameError = "Введіть прізвище",
                    _usernameError = "Виберіть адресу Gmail",
                    _confirmBlankError = "Підтвердьте пароль",
                    _confirmDontMatch = "Паролі не збігаються. Повторіть спробу.";


        [SetUp]
        public static void StartBrowser()
        {
            driver = new ChromeDriver();
            driver.Manage().Window.Maximize();
            driver.Url = "https://accounts.google.com/signup/v2/webcreateaccount?flowName=GlifWebSignIn&flowEntry=SignUp";
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

            wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(5);

            //firstName = driver.FindElement(By.Id("firstName"));
            //lastName = driver.FindElement(By.Id("lastName"));
            //username = driver.FindElement(By.Id("username"));
            //password = driver.FindElement(By.Name("Passwd"));
            //confirmPassword = driver.FindElement(By.Name("ConfirmPasswd"));
            //nextBtn = driver.FindElement(By.Id("accountDetailsNext"));
            //IList<IWebElement> errors = driver.FindElements(errorLocator);

        }


        public static void CreateAccount()
        {
            firstName = driver.FindElement(By.Id("firstName"));
            lastName = driver.FindElement(By.Id("lastName"));
            username = driver.FindElement(By.Id("username"));
            password = driver.FindElement(By.Name("Passwd"));
            confirmPassword = driver.FindElement(By.Name("ConfirmPasswd"));
            nextBtn = driver.FindElement(By.Id("accountDetailsNext"));

            firstName.SendKeys(_firstName);
            lastName.SendKeys(_lastName);
            username.SendKeys(_username);
            password.SendKeys(_password);
            confirmPassword.SendKeys(_password);
            nextBtn.Click();
            
            wait.Until(ExpectedConditions.UrlContains("webgradsidvphone"));
            wait.Until(ExpectedConditions.ElementIsVisible(header));
            confirmHeader = driver.FindElement(header);
            Assert.AreEqual(_confirmPhoneHeader, confirmHeader.Text);
            
            driver.Navigate().Back();
        }

        public static void BlankFirstName()
        {
            wait.Until(ExpectedConditions.ElementIsVisible(firstNameLocator));
            firstName = driver.FindElement(By.Id("firstName"));
            password = driver.FindElement(By.Name("Passwd"));
            confirmPassword = driver.FindElement(By.Name("ConfirmPasswd"));
            nextBtn = driver.FindElement(By.Id("accountDetailsNext"));

            firstName.Clear();
            password.SendKeys(_password);
            confirmPassword.SendKeys(_password);
            nextBtn.Click();

            wait.Until(ExpectedConditions.ElementIsVisible(errorLocator));
            IList<IWebElement> errors = driver.FindElements(errorLocator);
            error = errors[0];

            Assert.AreEqual(_firstNameError, error.Text);
        }

        public static void BlankLastName()
        {
            firstName = driver.FindElement(By.Id("firstName"));
            lastName = driver.FindElement(By.Id("lastName"));
            nextBtn = driver.FindElement(By.Id("accountDetailsNext"));

            firstName.SendKeys(_firstName);
            lastName.Clear();
            nextBtn.Click();

            wait.Until(ExpectedConditions.ElementIsVisible(errorLocator));
            IList<IWebElement> errors = driver.FindElements(errorLocator);
            error = errors[1];

            Assert.AreEqual(_lastNameError, error.Text);
        }

        public static void BlankUsername()
        {
            lastName = driver.FindElement(By.Id("lastName"));
            username = driver.FindElement(By.Id("username"));
            nextBtn = driver.FindElement(By.Id("accountDetailsNext"));

            lastName.SendKeys(_lastName);
            username.Clear();

            nextBtn.Click();

            wait.Until(ExpectedConditions.ElementIsVisible(errorLocator));
            IList<IWebElement> errors = driver.FindElements(errorLocator);
            error = errors[2];
            Assert.AreEqual(_usernameError, error.Text);
        }

        public static void PasswordCheck()
        {
            string inputText = password.Text;
            username.SendKeys(_username);

            // blank password
            password.Clear();
            nextBtn.Click();
            wait.Until(ExpectedConditions.ElementIsVisible(errorLocator));
            error = errors[3];
            Assert.AreEqual(PasswordWeak(inputText), error.Text);

            // short password
            password.SendKeys(_passwordShort);
            inputText = password.Text;
            nextBtn.Click();
            wait.Until(ExpectedConditions.ElementIsVisible(errorLocator));
            error = errors[3];

            Assert.AreEqual(PasswordWeak(inputText), error.Text);

            // weak password
            password.Clear();
            password.SendKeys(_passwordWeak);
            inputText = password.Text;
            error = errors[3];

            //wait.Until(ExpectedConditions.ElementIsVisible(passwordError));


            Assert.AreEqual(PasswordWeak(inputText), error.Text);
        }

        public static string PasswordWeak(string password)

        {
            Regex regex = new Regex(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[#$^+=!*()@%&]*)[a-zA-Z\d]{8,}$");
            MatchCollection matches = regex.Matches(password);

            if (password.Length == 0)
            {
                return "Уведіть пароль";
            }
            else if (password.Length < 8)
            {
                return "Пароль має містити щонайменше 8 символів";
            }
            else if (password.Length >= 8 && matches.Count == 0)
            {
                return "Виберіть надійніший пароль. Спробуйте комбінацію літер, цифр і символів.";
            }
            else
            {
                return "";
            }
        }

        [TearDown]
        public void CloseBrowser()
        {
            driver.Quit();
            driver = null;
        }
    }
}
